package in.sach.gamre.helpers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class EntityService {

    protected String entityPackageName;
    protected String packageName;
    protected String className;

    protected String entityClassName;
    protected String repositoryClassName;

    protected EntityService(EntityServiceBuilder serviceBuilder) {
        this.entityPackageName = serviceBuilder.entityPackageName;
        this.packageName = serviceBuilder.packageName;
        this.className = serviceBuilder.className;
        this.entityClassName = serviceBuilder.entityClassName;
        this.repositoryClassName = serviceBuilder.jpaRepositoryClassName;
    }

    protected static String getServicePackageName(String basePackageName) {
        String[] currentPackages = basePackageName.split("\\.");
        String[] topLevelPackages = Arrays.copyOf(currentPackages, currentPackages.length - 1);
        List<String> packageList = new ArrayList<String>(Arrays.asList(topLevelPackages));
        Collections.addAll(packageList, "service");
        return String.join(".", packageList);
    }

    public String getPackageName() {
        return packageName;
    }

    public String getClassName() {
        return className;
    }

    public String generateSourceCode() {
        boolean dataRepositoryPresent = false;
        if (repositoryClassName != null || repositoryClassName.length() > 0) {
            dataRepositoryPresent = true;
        }
        
        StringBuilder sourceCode = new StringBuilder()
                .append("package " + packageName + ";\n\n")

                .append("import " + entityPackageName + "." + entityClassName + ";\n")
                .append("import java.util.List;\n")
                .append("import java.util.ArrayList;\n")
                .append("import org.springframework.stereotype.Component;\n")
                .append("import org.springframework.beans.factory.annotation.Autowired;\n");
        
        
        if (dataRepositoryPresent) {
            sourceCode.append("import " + repositoryClassName + ";\n\n");
        }

        sourceCode.append("@Component \n")
                .append("public class " + className + " {\n");

        if (dataRepositoryPresent) {
            sourceCode.append("@Autowired\n")
                    .append("private " + repositoryClassName + " entityRepository;\n");
        }
        sourceCode.append("   public List<" + entityClassName + "> create(List<" + entityClassName + "> createEntities) {\n");
        if (dataRepositoryPresent) {
        	sourceCode.append("List<" + entityClassName +"> createdList = new ArrayList<" + entityClassName + ">();\n")
        	.append("for (" + entityClassName + " entity: createEntities) {\n")
            .append("		createdList.add(entityRepository.save(entity));\n")
            .append("}\n")
            .append("return createdList;");
        } else {
        	sourceCode.append("return null;");
        }
        sourceCode.append("   }\n\n")
            .append("   public List<" + entityClassName + "> update(List<" + entityClassName + "> updateEntities) {\n");
        if (dataRepositoryPresent) {
        	sourceCode.append("List<" + entityClassName +"> updatedList = new ArrayList<" + entityClassName + ">();\n");
        	sourceCode.append("for (" + entityClassName + " entity: updateEntities) {\n");
            sourceCode.append("		updatedList.add(entityRepository.save(entity));\n");
            sourceCode.append("}\n")
            .append("return updatedList;");
        } else {
        	sourceCode.append("return null;");
        }
        sourceCode.append("   }\n\n")
            .append("   public List<" + entityClassName + "> getAll() {\n");
        if (dataRepositoryPresent) {
            sourceCode.append("return entityRepository.findAll();\n");
        } else {
        	sourceCode.append("return null;");
        }
         sourceCode.append("   }\n\n")
         .append("   public void delete(" + entityClassName + " entity) {\n");
         if (dataRepositoryPresent) {
             sourceCode.append("entityRepository.delete(entity);\n");
         }
         sourceCode.append("   }\n\n")
            .append("}\n");

        return sourceCode.toString();
    }
    
}
