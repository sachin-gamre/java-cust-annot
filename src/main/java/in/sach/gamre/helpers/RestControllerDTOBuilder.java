package in.sach.gamre.helpers;

import java.lang.reflect.Field;

public class RestControllerDTOBuilder {

    protected String actualPackageName;
    protected String entityClassName;
    protected String packageName;
    protected String className;
    protected Field[] memberVariables;

    protected RestControllerDTOBuilder(String entityPackageName, String entityClassName, SourceCode.DTOType dtoType, Field[] memberVariables) {
        if (entityClassName == null || "".equalsIgnoreCase(entityClassName)
                || entityClassName.length() == 0 || entityPackageName == null
                || "".equalsIgnoreCase(entityPackageName) || entityPackageName.length() == 0) {
            throw new IllegalArgumentException("Missing entityPackageName or entityClassName");
        }

        this.actualPackageName = entityPackageName;
        this.entityClassName = entityClassName;
        this.memberVariables = memberVariables;
        this.packageName = RestController.getControllerPackageName(entityPackageName) + ".dto";
        switch (dtoType) {
            case CREATE:
                this.className = entityClassName + "RequestDTO";
                break;
            case UPDATE:
                this.className = entityClassName + "UpdateDTO";
                break;
            default:
                this.className = entityClassName;
        }
    }

    public RestControllerDTO build() {
        RestControllerDTO controllerDTO = new RestControllerDTO(this);
        return controllerDTO;
    }
}
