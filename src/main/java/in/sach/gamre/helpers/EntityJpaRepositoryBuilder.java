package in.sach.gamre.helpers;

public class EntityJpaRepositoryBuilder {

    protected String entityPackageName;
    protected String packageName;
    protected String className;

    protected String entityClassName;
    protected String repositoryClassName;
    protected String primaryKeyQualifiedClassName;
    
    protected EntityJpaRepositoryBuilder(String entityPackageName, String entityClassName, String pkQualifiedClassName) {
        if (entityClassName == null || "".equalsIgnoreCase(entityClassName)
                || entityClassName.length() == 0 || entityPackageName == null
                || "".equalsIgnoreCase(entityPackageName) || entityPackageName.length() == 0
                || pkQualifiedClassName == null || "".equalsIgnoreCase(pkQualifiedClassName)) {
            throw new IllegalArgumentException("Missing entityPackageName or entityClassName");
        }

        this.entityPackageName = entityPackageName;
        this.packageName = EntityJpaRepository.getRepositoryPackageName(entityPackageName);
        this.className = entityClassName;
        this.repositoryClassName = entityClassName + "Repository";
        this.entityClassName = entityClassName;
        this.primaryKeyQualifiedClassName = pkQualifiedClassName;
    }

    public EntityJpaRepository build() {
    	EntityJpaRepository repository = new EntityJpaRepository(this);
        return repository;
    }
    
}
