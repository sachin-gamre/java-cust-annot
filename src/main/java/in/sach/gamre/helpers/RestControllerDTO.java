package in.sach.gamre.helpers;

import java.lang.reflect.Field;

public class RestControllerDTO {

    private String actualPackageName;
    private String entityClassName;
    private String packageName;
    private String className;
    private Field[] memberVariables;

    protected RestControllerDTO(RestControllerDTOBuilder dtoBuilder) {
        this.packageName = dtoBuilder.packageName;
        this.className = dtoBuilder.className;
        this.actualPackageName = dtoBuilder.actualPackageName;
        this.entityClassName = dtoBuilder.entityClassName;
        this.memberVariables = dtoBuilder.memberVariables;
    }

    public String getPackageName() {
        return this.packageName;
    }

    public String getClassName() {
        return this.className;
    }

    public String getSource() {
        StringBuilder sourceCode = new StringBuilder()
                .append("package " + packageName + ";\n\n")
                .append("public class " + className + " {\n");
        for(Field f : memberVariables) {
            sourceCode.append("private " + f.getType().getSimpleName() + " " + f.getName() + ";\n\n");
            sourceCode.append("public void set" + RestController.capitalizeFirst(f.getName()) + "(" + f.getType().getSimpleName() + " value) {\n");
            sourceCode.append(" this." + f.getName() + " = value;\n");
            sourceCode.append("}\n\n");

            sourceCode.append("public " + f.getType().getSimpleName() + " get" + RestController.capitalizeFirst(f.getName()) + "() {\n");
            sourceCode.append(" return this." + f.getName() + ";\n");
            sourceCode.append("}\n\n");
        }
        sourceCode.append("}\n\n");
        return sourceCode.toString();
    }
}
