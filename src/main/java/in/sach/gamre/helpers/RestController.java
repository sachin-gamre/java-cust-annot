package in.sach.gamre.helpers;


import java.util.*;

public class RestController {

    private String entityClassName;
    private String entityPackageName;

    private String packageName;
    private String className;
    private String mappingPrefix;
    private String createMapping;
    private String updateMapping;
    private String getMapping;
    private String deleteMapping;
    private String serviceName;

    protected RestController(RestControllerBuilder restControllerBuilder) {
        this.entityPackageName = restControllerBuilder.entityPackageName;
        this.packageName = restControllerBuilder.packageName;
        this.className = restControllerBuilder.className;
        this.mappingPrefix = restControllerBuilder.mappingPrefix;
        this.createMapping = restControllerBuilder.createMapping;
        this.updateMapping = restControllerBuilder.updateMapping;
        this.getMapping = restControllerBuilder.getMapping;
        this.deleteMapping = restControllerBuilder.deleteMapping;
        this.entityClassName = restControllerBuilder.entityClassName;
        this.serviceName = restControllerBuilder.qualifiedServiceName;
    }

    protected static String getControllerPackageName(String currentPackageName) {
        String[] currentPackages = currentPackageName.split("\\.");
        String[] topLevelPackages = Arrays.copyOf(currentPackages, currentPackages.length - 1);
        List<String> packageList = new ArrayList(Arrays.asList(topLevelPackages));
        Collections.addAll(packageList, "controller");
        return String.join(".", packageList);
    }

    protected static String capitalizeFirst(String str) {
        if(str == null) return str;
        return str.substring(0, 1).toUpperCase() + str.substring(1);
    }

    public String getPackageName() {
        return packageName;
    }

    public String getClassName() {
        return className;
    }

    public String getSource() {

        StringBuilder sourceCode = new StringBuilder()
                .append("package " + packageName + ";\n\n")

                .append("import " + entityPackageName + "." + entityClassName + ";\n")
                .append("import org.springframework.beans.factory.annotation.Autowired;\n")
                .append("import org.springframework.web.bind.annotation.RequestMapping;\n")
                .append("import org.springframework.web.bind.annotation.RequestMethod;\n")
                .append("import org.springframework.web.bind.annotation.RequestBody;\n")
                .append("import org.springframework.web.bind.annotation.RestController;\n")
		        .append("import org.springframework.web.bind.annotation.RequestBody;\n")
		        .append("import org.springframework.web.bind.annotation.RequestParam;\n")
		        .append("import org.springframework.web.bind.annotation.ResponseBody;\n")
		        .append("import java.util.List;\n");

                if (serviceName != null && !"".equalsIgnoreCase(serviceName)) {
                	sourceCode.append("import " + serviceName + ";\n\n");
                }
                
                sourceCode.append("@RestController \n")
                .append("@RequestMapping(path=\"" + mappingPrefix + "\")\n")
                .append("public class " + className + " {\n");
		        if (serviceName != null && !"".equalsIgnoreCase(serviceName)) {
		        	sourceCode.append("@Autowired private " + serviceName + " entityService;\n");
		        }
                sourceCode.append("   @RequestMapping(value = \"" + createMapping + "\", method = RequestMethod.POST)")
                .append("   public @ResponseBody List<" + entityClassName + "> create(@RequestBody List<" + entityClassName + "> createEntityList) {\n");
                if (serviceName != null && !"".equalsIgnoreCase(serviceName)) {
                	sourceCode.append(" return entityService.create(createEntityList);\n");
                } else {
                	sourceCode.append("return null;");
                }
                sourceCode.append("   }\n\n")

                .append("   @RequestMapping(value = \"" + updateMapping + "\", method = RequestMethod.PUT)")
                .append("   public @ResponseBody List<" + entityClassName + "> update(@RequestBody List<" + entityClassName + "> updateEntityList) {\n");
                if (serviceName != null && !"".equalsIgnoreCase(serviceName)) {
                	sourceCode.append(" return entityService.update(updateEntityList);\n");
                } else {
                	sourceCode.append("return null;");
                }
                sourceCode.append("   }\n\n")

                .append("   @RequestMapping(value = \"" + getMapping + "\", method = RequestMethod.GET)")
                .append("   public @ResponseBody List<" + entityClassName + "> getAll() {\n");
                if (serviceName != null && !"".equalsIgnoreCase(serviceName)) {
                	sourceCode.append(" return entityService.getAll();\n");
                } else {
                	sourceCode.append("return null;");
                }
                sourceCode.append("   }\n\n")
                .append("   @RequestMapping(value = \"" + deleteMapping + "\", method = RequestMethod.DELETE)")
                .append("   public @ResponseBody Boolean delete(" + entityClassName + " entityToDelete) {\n");
                if (serviceName != null && !"".equalsIgnoreCase(serviceName)) {
                	sourceCode.append(" entityService.delete(entityToDelete);\n");
                }
                sourceCode.append("   return null; }\n\n");
                sourceCode.append("   }\n\n");

        return sourceCode.toString();
    }
}
