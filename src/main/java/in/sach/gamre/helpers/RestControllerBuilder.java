package in.sach.gamre.helpers;

public class RestControllerBuilder {

    protected String entityPackageName;
    protected String packageName;
    protected String className;

    protected String mappingPrefix;
    protected String createMapping;
    protected String updateMapping;
    protected String getMapping;
    protected String deleteMapping;
    protected String qualifiedServiceName;

    protected String entityClassName;


    protected RestControllerBuilder(String entityPackageName, String entityClassName) {
        if (entityClassName == null || "".equalsIgnoreCase(entityClassName)
                || entityClassName.length() == 0 || entityPackageName == null
                || "".equalsIgnoreCase(entityPackageName) || entityPackageName.length() == 0) {
            throw new IllegalArgumentException("Missing entityPackageName or entityClassName");
        }

        this.entityPackageName = entityPackageName;
        this.packageName = RestController.getControllerPackageName(entityPackageName);
        this.className = entityClassName + "Controller";
        this.entityClassName = entityClassName;
        this.mappingPrefix = "/" + entityClassName;
        this.createMapping = "/create";
        this.updateMapping = "/update";
        this.getMapping = "/list";
        this.deleteMapping = "/delete";
    }

    public RestControllerBuilder setMappingPrefix(String mappingPrefix) {
        this.mappingPrefix = mappingPrefix;
        return this;
    }

    public RestControllerBuilder setCreateMapping(String createMapping) {
        this.createMapping = createMapping;
        return this;
    }

    public RestControllerBuilder setUpdateMapping(String updateMapping) {
        this.updateMapping = updateMapping;
        return this;
    }

    public RestControllerBuilder setGetMapping(String getMapping) {
        this.getMapping = getMapping;
        return this;
    }
    
    public RestControllerBuilder setDeleteMapping(String deleteMapping) {
        this.deleteMapping = deleteMapping;
        return this;
    }
    
    public RestControllerBuilder setQualifiedServiceName(String serviceName) {
        this.qualifiedServiceName = serviceName;
        return this;
    }

    public RestController build() {
        RestController restController = new RestController(this);
        return restController;
    }
}
