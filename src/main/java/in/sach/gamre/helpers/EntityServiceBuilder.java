package in.sach.gamre.helpers;

public class EntityServiceBuilder {

    protected String entityPackageName;
    protected String packageName;
    protected String className;

    protected String entityClassName;
    protected String jpaRepositoryClassName;

    protected EntityServiceBuilder(String entityPackageName, String entityClassName) {
        if (entityClassName == null || "".equalsIgnoreCase(entityClassName)
                || entityClassName.length() == 0 || entityPackageName == null
                || "".equalsIgnoreCase(entityPackageName) || entityPackageName.length() == 0) {
            throw new IllegalArgumentException("Missing entityPackageName or entityClassName");
        }

        this.entityPackageName = entityPackageName;
        this.packageName = EntityService.getServicePackageName(entityPackageName);
        this.className = entityClassName + "Service";
        this.entityClassName = entityClassName;
    }

    public EntityServiceBuilder setJpaRepositoryClassName(String jpaRepositoryClassName) {
        this.jpaRepositoryClassName = jpaRepositoryClassName;
        return this;
    }

    public EntityService build() {
        EntityService service = new EntityService(this);
        return service;
    }
}
