package in.sach.gamre.helpers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class EntityJpaRepository {
	
	protected String entityPackageName;
    protected String packageName;
    protected String className;

    protected String entityClassName;
    protected String repositoryClassName;
    protected String primaryKeyQualifiedClassName;
    
    private static List<String> javaDataType = new ArrayList<String>();
	
	static {
		
		javaDataType.add("Byte");
		javaDataType.add("Short");
		javaDataType.add("Integer");
		javaDataType.add("Long");
		javaDataType.add("Float");
		javaDataType.add("Double");
		javaDataType.add("Boolean");
		javaDataType.add("Character");
		
	}

    protected EntityJpaRepository(EntityJpaRepositoryBuilder repositoryBuilder) {
        this.entityPackageName = repositoryBuilder.entityPackageName;
        this.packageName = repositoryBuilder.packageName;
        this.className = repositoryBuilder.className;
        this.repositoryClassName = repositoryBuilder.repositoryClassName;
        this.entityClassName = repositoryBuilder.entityClassName;
        this.primaryKeyQualifiedClassName = repositoryBuilder.primaryKeyQualifiedClassName;
    }

    protected static String getRepositoryPackageName(String basePackageName) {
        String[] currentPackages = basePackageName.split("\\.");
        String[] topLevelPackages = Arrays.copyOf(currentPackages, currentPackages.length - 1);
        List<String> packageList = new ArrayList(Arrays.asList(topLevelPackages));
        Collections.addAll(packageList, "repository");
        return String.join(".", packageList);
    }

    public String getPackageName() {
        return packageName;
    }

    public String getClassName() {
        return repositoryClassName;
    }

    /**
     * Generates source code for repository java class.
     *
     * @param packageName
     * @param repositoryClassName
     * @param modelClassName
     * @param primaryKeyQualifiedClassName
     * @return
     */
    public String generateSourceCode() {
    	
        StringBuilder sourceCode = new StringBuilder()
                .append("package " + packageName + ";\n\n")
                .append("import org.springframework.data.jpa.repository.JpaRepository; \n")
                .append("import org.springframework.stereotype.Repository;\n")
                .append("import " + entityPackageName + "." + entityClassName + ";\n");
        
        
		        if (!javaDataType.contains(primaryKeyQualifiedClassName)) {
		        	sourceCode.append("import " +  primaryKeyQualifiedClassName + "; \n\n");
		        }
                
                sourceCode.append("@Repository\n")
                .append("public interface " + repositoryClassName + " extends JpaRepository<" + entityClassName + ", " + primaryKeyQualifiedClassName + "> {\n")
                .append("}\n");

        return sourceCode.toString();
    }
	
}
