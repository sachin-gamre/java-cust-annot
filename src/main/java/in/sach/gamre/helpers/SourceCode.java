package in.sach.gamre.helpers;

import javax.tools.JavaFileObject;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Field;
import java.util.Arrays;

/**
 * Source code generator class.
 *
 * @author Sachin Janardan Gamre <sachin.gamre@gmail.com>
 */
public class SourceCode {

    public enum DTOType {
        CREATE,
        UPDATE
    };

    public static RestControllerBuilder restControllerBuilder(String entityPackageName, String entityClassName) {
        return new RestControllerBuilder(entityPackageName, entityClassName);
    }

    public static EntityServiceBuilder entityServiceBuilder(String entityPackageName, String entityClassName) {
        return new EntityServiceBuilder(entityPackageName, entityClassName);
    }
    
    public static EntityJpaRepositoryBuilder entityJpaRepositoryBuilder(String entityPackageName, String entityClassName, String pkQualifiedName) {
        return new EntityJpaRepositoryBuilder(entityPackageName, entityClassName, pkQualifiedName);
    }
    
}
