package in.sach.gamre.processors;

import javax.annotation.processing.AbstractProcessor;
import javax.tools.JavaFileObject;
import java.io.IOException;
import java.io.PrintWriter;

public abstract class GenerateAnnotationsAbstractProcessor extends AbstractProcessor {

    protected void writeToJavaFile(String fileName, String sourceCode) throws IOException {
        JavaFileObject builderFile = null;
        builderFile = processingEnv.getFiler().createSourceFile(fileName);

        // Write source code for JpaRepository.
        try (PrintWriter out = new PrintWriter(builderFile.openWriter())) {
            out.write(sourceCode);
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
            throw e;
        }
    }
}
