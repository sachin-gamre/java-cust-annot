package in.sach.gamre.processors;

import in.sach.gamre.annotations.GenerateEntityService;
import in.sach.gamre.annotations.GenerateRestController;
import in.sach.gamre.helpers.EntityService;
import in.sach.gamre.helpers.RestController;
import in.sach.gamre.helpers.SourceCode;

import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedAnnotationTypes;
import javax.annotation.processing.SupportedSourceVersion;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.PackageElement;
import javax.lang.model.element.TypeElement;
import javax.lang.model.util.ElementFilter;
import java.io.IOException;
import java.util.Collection;
import java.util.List;
import java.util.Set;

@SupportedAnnotationTypes("in.sach.gamre.annotations.GenerateEntityService")
@SupportedSourceVersion(SourceVersion.RELEASE_8)
public class GenerateEntityServiceAnnotationProcessor extends GenerateAnnotationsAbstractProcessor {

    @Override
    public boolean process(Set<? extends TypeElement> set, RoundEnvironment roundEnvironment) {
        Collection<? extends Element> annotatedElements = roundEnvironment.getElementsAnnotatedWith(GenerateEntityService.class);
        List<TypeElement> types = ElementFilter.typesIn(annotatedElements);

        String packageName = null;
        String repositoryClassName = null;
        String modelClassName = null;

        // Grab the packageName
        for (TypeElement type: types) {
            PackageElement packageElement = (PackageElement) type.getEnclosingElement();
            packageName = packageElement.getQualifiedName().toString();

            modelClassName = type.getSimpleName().toString();
            repositoryClassName = type.getAnnotation(GenerateEntityService.class).repositoryClassName();

            if (packageName == null) {
                continue;
            }

            try {
                EntityService entityService = SourceCode.entityServiceBuilder(packageName, modelClassName)
                        .setJpaRepositoryClassName(repositoryClassName)
                        .build();
                String serviceFile = entityService.getPackageName() + "." + entityService.getClassName();
                writeToJavaFile(serviceFile, entityService.generateSourceCode());

            } catch (IOException e) {
                e.printStackTrace();
                continue;
            }
        }
        return true;
    }
}
