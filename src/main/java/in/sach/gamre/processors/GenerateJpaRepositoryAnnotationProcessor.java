package in.sach.gamre.processors;

import java.io.IOException;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedAnnotationTypes;
import javax.annotation.processing.SupportedSourceVersion;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.PackageElement;
import javax.lang.model.element.TypeElement;
import javax.lang.model.util.ElementFilter;

import in.sach.gamre.annotations.GenerateJpaRepository;
import in.sach.gamre.helpers.EntityJpaRepository;
import in.sach.gamre.helpers.SourceCode;

/**
 * MyJpaRepository Annotation Processor
 *
 * @author Sachin Janardan Gamre <sachin.gamre@gmail.com>
 */
@SupportedAnnotationTypes("in.sach.gamre.annotations.GenerateJpaRepository")
@SupportedSourceVersion(SourceVersion.RELEASE_8)
public class GenerateJpaRepositoryAnnotationProcessor extends GenerateAnnotationsAbstractProcessor {

    /**
     * Process method to actually process the annotation.
     * For every model class, annotated with @MyJpaRepository annotation
     * it creates the JpaRepository class.
     * @param set
     * @param roundEnvironment
     * @return
     */
    @Override
    public boolean process(Set<? extends TypeElement> set, RoundEnvironment roundEnvironment) {
        Collection<? extends Element> annotatedElements = roundEnvironment.getElementsAnnotatedWith(GenerateJpaRepository.class);
        List<TypeElement> types = ElementFilter.typesIn(annotatedElements);

        String packageName = null;
        String modelClassName = null;
        String primaryKeyQualifiedClassName = null;

        // Grab the packageName
        for (TypeElement type: types) {
            PackageElement packageElement = (PackageElement) type.getEnclosingElement();
            packageName = packageElement.getQualifiedName().toString();
            primaryKeyQualifiedClassName = type.getAnnotation(GenerateJpaRepository.class).pkQualifiedClassName();
            modelClassName = type.getSimpleName().toString();

            if (packageName == null) {
                continue;
            }
            
            try {
            	EntityJpaRepository jpaRepository = SourceCode.entityJpaRepositoryBuilder(packageName, modelClassName, primaryKeyQualifiedClassName)
            			.build();
            	String jpaRepositoryFile = jpaRepository.getPackageName() + "." + jpaRepository.getClassName();
            	writeToJavaFile(jpaRepositoryFile, jpaRepository.generateSourceCode());
            } catch (IOException e) {
            	e.printStackTrace();
            	continue;
            }
            
        }

        return true;
    }
    
}
