package in.sach.gamre.processors;

import in.sach.gamre.annotations.GenerateRestController;
import in.sach.gamre.helpers.RestController;
import in.sach.gamre.helpers.RestControllerDTO;
import in.sach.gamre.helpers.SourceCode;

import javax.annotation.processing.*;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.PackageElement;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.VariableElement;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.util.ElementFilter;
import javax.tools.Diagnostic;
import javax.tools.JavaFileObject;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Field;
import java.util.*;


@SupportedAnnotationTypes("in.sach.gamre.annotations.GenerateRestController")
@SupportedSourceVersion(SourceVersion.RELEASE_8)
public class GenerateRestControllerAnnotationProcessor extends GenerateAnnotationsAbstractProcessor {

    public synchronized void init(ProcessingEnvironment processingEnvironment) {
        super.init(processingEnvironment);
    }

    @Override
    public boolean process(Set<? extends TypeElement> set, RoundEnvironment roundEnvironment) {
        System.out.println("Set size " + set.size());
        Collection<? extends Element> annotatedElements = roundEnvironment.getElementsAnnotatedWith(GenerateRestController.class);
        List<TypeElement> types = ElementFilter.typesIn(annotatedElements);

        String packageName = null;
        String mappingPrefix = null;
        String createMapping = null;
        String updateMapping = null;
        String getMapping = null;
        String deleteMapping = null;
        String modelClassName = null;
        String qualifiedServiceName = null;

        // Grab the packageName
        for (TypeElement type: types) {
            PackageElement packageElement = (PackageElement) type.getEnclosingElement();
            packageName = packageElement.getQualifiedName().toString();

            modelClassName = type.getSimpleName().toString();
            mappingPrefix = type.getAnnotation(GenerateRestController.class).mappingPrefix();
            createMapping = type.getAnnotation(GenerateRestController.class).createApiMapping();
            updateMapping = type.getAnnotation(GenerateRestController.class).updateApiMapping();
            getMapping = type.getAnnotation(GenerateRestController.class).getApiMapping();
            deleteMapping = type.getAnnotation(GenerateRestController.class).deleteApiMapping();
            qualifiedServiceName = type.getAnnotation(GenerateRestController.class).qualifiedServiceName();
            

            if (packageName == null) {
                continue;
            }

            try {
                RestController restController = SourceCode.restControllerBuilder(packageName, modelClassName)
                        .setMappingPrefix(mappingPrefix)
                        .setCreateMapping(createMapping)
                        .setUpdateMapping(updateMapping)
                        .setGetMapping(getMapping)
                        .setQualifiedServiceName(qualifiedServiceName)
                        .build();
                String controllerFile = restController.getPackageName() + "." + restController.getClassName();
                writeToJavaFile(controllerFile, restController.getSource());

            } catch (IOException e) {
                e.printStackTrace();
                continue;
            }
        }
        return true;
    }
}
