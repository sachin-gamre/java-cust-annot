package in.sach.gamre.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.SOURCE)
@Target(ElementType.TYPE)
public @interface GenerateJpaRepository {
	
	boolean overwrite() default false;
	
	/**
	 * Mandatory Primary key field.
	 * 
	 * @return
	 */
    String pkQualifiedClassName();
}
