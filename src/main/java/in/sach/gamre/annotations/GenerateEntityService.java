package in.sach.gamre.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.SOURCE)
@Target(ElementType.TYPE)
public @interface GenerateEntityService {
	
	boolean overwrite() default false;
	
	/**
	 * Repository class name is mandatory field.
	 * 
	 * @return
	 */
    String repositoryClassName();
    
}
