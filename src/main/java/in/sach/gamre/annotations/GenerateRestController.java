package in.sach.gamre.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.SOURCE)
@Target(ElementType.TYPE)
public @interface GenerateRestController {
	
	boolean overwrite() default false;
	
	/**
	 * API resource prefix.
	 * 
	 * @return
	 */
    String mappingPrefix();
    
    /**
     * Create new endpoint
     * 
     * @return
     */
    String createApiMapping() default "/new";
    
    /**
     * Update endpoint.
     * 
     * @return
     */
    String updateApiMapping() default "/update";
    
    /**
     * Get endpoint
     * 
     * @return
     */
    String getApiMapping() default "/list";
    
    /**
     * delete endpoint
     * 
     * @return
     */
    String deleteApiMapping() default "/delete";
    
    /**
     * fully qualified name of Service Bean.
     * 
     * @return
     */
    String qualifiedServiceName() default "";
    
}
