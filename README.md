Java Web Annotations
=========================

Annotations present in this project are helper annotations for creating a functional spring based web app. The vision is to reduce the development time and get application ready by just defining domain entities and use of the annotation.

The project has below mentioned annotations available

1. @GenerateJpaRepository

Add this annotation to create a JpaRepository class for given entity.

2. @GenerateEntityService

Add this annotation to create a service class for given entity.

3. @GenerateRestController

Add this annotation to create a service class for given entity.






